import React from "react";
import './index.css';
import img1 from './images/about-1.jpg';
import img2 from './images/about-2.jpg';
import img3 from './images/about-3.jpg';

const About = () => {
    return(
        <div className='about__wrapper'>
            <h3 className='about__title'>
                Наші самі великі проєкти
            </h3>
            <div className='about__content'>
                <div className='about__item'>
                    <img className='about__img' src={img1}/>
                    <span></span>
                    <p className='about__item-title'>Земля</p>
                    <p className='about__item-text'>
                    третья по удалённости от Солнца планета Солнечной системы.
                    Самая плотная, пятая по диаметру и массе среди всех планет
                    и крупнейшая среди планет земной группы, в которую входят
                    также Меркурий, Венера и Марс. Единственное известное человеку
                    в настоящее время тело Солнечной системы в частности и Вселенной
                    вообще, населённое живыми организмами.
                    </p>
                </div>
                <div className='about__item'>
                    <img className='about__img' src={img2}/>
                    <span></span>
                    <p className='about__item-title'>Марс</p>
                    <p className='about__item-text'>
                    третья по удалённости от Солнца планета Солнечной системы.
                    Самая плотная, пятая по диаметру и массе среди всех планет
                    и крупнейшая среди планет земной группы, в которую входят
                    также Меркурий, Венера и Марс. Единственное известное человеку
                    в настоящее время тело Солнечной системы в частности и Вселенной
                    вообще, населённое живыми организмами.
                    </p>
                </div>
                <div className='about__item'>
                    <img className='about__img' src={img3}/>
                    <span></span>
                    <p className='about__item-title'>Сатурн</p>
                    <p className='about__item-text'>
                    третья по удалённости от Солнца планета Солнечной системы.
                    Самая плотная, пятая по диаметру и массе среди всех планет
                    и крупнейшая среди планет земной группы, в которую входят
                    также Меркурий, Венера и Марс. Единственное известное человеку
                    в настоящее время тело Солнечной системы в частности и Вселенной
                    вообще, населённое живыми организмами.
                    </p>
                </div>
            </div>
        </div>
    )
}



export default About