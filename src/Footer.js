import React from "react";
import './index';
import img1 from './images/email.svg';

const Footer = () => {
    return(
        <div className='footer__wrapper'>
            <div className='container'>
                <div className='footer__content'>
                    <div className='footer__text'>
                        <p className='footer__title'>САМІ РОЗУМНІ ПРОЄКТИ</p>
                        <p className='footer__subtitle'>РОЗРОБЛЮЄМО САМІ СМІЛИВІ ПРОЕКТИ В УКРАЇНІ</p>
                    </div>
                    <div className='footer__email'>
                        <img src={img1}/>
                        <p className='footer__email-text'>ВАШ ЗАПИТ</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer;