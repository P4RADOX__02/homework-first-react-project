import React from "react";
import './index.css';
import img1 from './images/logo.svg';
import img2 from './images/menu-gamburger.svg';

const Header = () => {
    return (
        <div>
            <div className='header__top'>
                <img className='logo' src={img1}/>
                <div className='menu'>
                    <button className='header__btn'>
                        <img className='menu-logo' src={img2}/>
                    </button>
                </div>
            </div>
            <div className='header__content'>
                <p className='header__title'>
                СТВОРЮЄМО ВЕЛИКІ
                ПРОЄКТИ В УКРАЇНІ
                </p>
                <p className='header__subtitle'>стадионы, газопроводы, мосты, дамбы</p>
            </div>
        </div>
    )
}

export default Header;