import React from "react";
import './index.css';
import Header from './Header';
import Numbers from './Numbers';
import About from './About';
import Footer from './Footer';

const App = () => {
    return (
        <div className='app-wrapper'>
            <div className='container'>
                <Header />
                <Numbers />
                <About />
            </div>
            <Footer />
        </div>
    )
}

export default App;