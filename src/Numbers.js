import React from "react";

const Numbers = () => {
    return(
        <div className='numbers__wrapper'>
            <div className='numbers__content'>
                <div className='numbers__item'>
                    <p className='number'>10</p>
                    <p className='numbers__title'>Років</p>
                    <p className='numbers__subtitle'>з вами</p>
                </div>
                <div className='numbers__item'>
                    <p className='number'>500+</p>
                    <p className='numbers__title'>Проєктів</p>
                    <p className='numbers__subtitle'>зроблено</p>
                </div>
                <div className='numbers__item'>
                    <p className='number'>400+</p>
                    <p className='numbers__title'>клієнтів</p>
                    <p className='numbers__subtitle'>в захваті</p>
                </div>
                <div className='numbers__item'>
                    <p className='number'>30</p>
                    <p className='numbers__title'>Хвилин</p>
                    <p className='numbers__subtitle'>презентація</p>
                </div>
            </div>
        </div>
    )
}

export default Numbers;